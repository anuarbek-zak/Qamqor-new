function showSpeedometer(marker, color) {
	var values = [0, 15, 50, 85];
	//var colors = ["black", "rgb(68, 209, 45)", "rgb(127, 193, 253)", "rgb(251, 160, 155)"]; //OLD
	var colors = ["black", "rgb(127, 193, 253)", "rgb(252, 203, 200)", "rgb(247, 69, 59)"];
	var texts = ["N/A", "Низкий", "Средний", "Высокий"];
	
	var chart = c3.generate({
		bindto: '#chart_' + marker,
		data: {
			columns: [
				['data', values[color]]
			],
			type: 'gauge'
		},
		tooltip: {
			show: false
		},
		gauge: {
			label: {
				format: function(value, ratio) {
					return texts[color];
				},
				show: false
			},
			min: 0, // 0 is default, //can handle negative min e.g. vacuum / voltage / current flow / rate of change
			max: 100, // 100 is default
			units: ' %',
			width: 25 // for adjusting arc thickness
		},
		color: {
			pattern: [colors[color]]
		},
		size: {
			height: 80
		}
	});
}