/**
 * Author: Шестаков П.Н. и Курмангалиев Е.Ж. 
 * Script: Карта для статистики
 */
define([
    "dojo/_base/lang",
    "dojo/dom",
    "dojo/dom-style",
    "dojo/dom-construct",
    "dojo/query",
    "dojo/on",
    "dojo/aspect",
    "dojo/Deferred",
    "dijit/registry",

    "esri/map",
    "esri/geometry/Extent",
    "esri/layers/ArcGISTiledMapServiceLayer",
    "esri/tasks/QueryTask",
    "esri/tasks/query",
    "esri/layers/GraphicsLayer",

    "esri/graphic",
    "esri/symbols/SimpleFillSymbol",
    "esri/Color",
    "esri/renderers/ClassBreaksRenderer",
    "esri/symbols/TextSymbol",
    "esri/symbols/Font",
    "esri/symbols/SimpleLineSymbol",
    "dojox/charting/Chart2D",
    "dojo/fx/easing",
    "dojox/charting/themes/ThreeD",
    "dijit/popup",
    "dijit/TooltipDialog",
    "dijit/form/CheckBox",
    "dojo/request/xhr",
    "dojox/charting/action2d/Tooltip",
    "analytics/Util",
	"esri/geometry/Extent",
	"dojox/charting/plot2d/Pie",
	"dojox/charting/action2d/Magnify",
	"dojox/charting/action2d/Highlight"
], function (lang, dom, domStyle, domConstruct, query, on, aspect, Deferred, registry, Map, Extent, ArcGISTiledMapServiceLayer, QueryTask, qr, GraphicsLayer, Graphic, SimpleFillSymbol, Color, ClassBreaksRenderer, TextSymbol, Font, SimpleLineSymbol, Chart2D, easing, chThemesThreeD, DijitPopup, TooltipDialog, CheckBox, xhr, Tooltip, Util, Point, Pie, Magnify, Highlight) {

    var map, queryTask, queryGIS, d, dialog, lineChart;
    var Cities = ['191110', '191910', '191926', '193516', '193518', '193520', '193522', '193523', '193524', '193528', '195510', '195910', '193920', '196324', '191510', '191118', '193521', '194319', '195116'];
	
	var ias3url = "http://projects.kurmangaliyev.kz/GenProsecutorOffice-3-Beta/";
	
	$(document).ajaxError(function(ev, xhr, settings) {
		console.error("Не удалось загрузить " + settings.url + ". Ответ сервера: " + xhr.status + "(" + xhr.responseText + ")");
	});
	
	var defaultColors1 = ["#C7C7C7", "rgb(68, 209, 45)", "rgb(127, 193, 253)", "rgb(251, 160, 155)"]; //OLD
	var defaultColors = ["#C7C7C7", "rgb(127, 193, 253)", "rgb(252, 203, 200)", "rgb(247, 69, 59)"]; //NEW
	
	var clustersCount = 14;
	
	if (document.location.href.indexOf("?") > -1) {
		var argumentsString = document.location.href.substr(document.location.href.indexOf("?") + 1);
		
		if (argumentsString.length) {
			argumentsString.split("&").map(function(x) {
				return { 
					key: x.split("=")[0],
					value: x.split("=")[1]
				}; 
			}).forEach(function(arg) {
				switch (arg.key) {
					case "clusters":
						clustersCount = +arg.value;
						var $dropdownCluster = $("#dropdownCluster").empty();
						$("<option/>").text("Группы схожих райнов").attr("value", 0).appendTo($dropdownCluster);
						for (var i = 1; i <= clustersCount; i++)
						{
							$("<option/>").text(i + ": Группа " + i).attr("value", i).appendTo($dropdownCluster);
						}
						break;
						
					case "verbose":
						window.$log = $("<div/>").appendTo("body");
						
						var consoleLog = console.log,
							consoleError = console.error,
							consoleDebug = console.debug;
							
						console.log = function(x) {
							$("<p/>").text(x).appendTo(window.$log);
							consoleLog(x);
						};
						
						console.debug = function(x) {
							$("<p/>").css("color", "blue").text(x).appendTo(window.$log);
							consoleDebug(x);
						};
						
						console.error = function(x) {
							$("<p/>").css("color", "red").text(x).appendTo(window.$log);
							consoleError(x);
						};
						
						window.onerror = function(msg, url, lineNo, columnNo, error) {
							var message = [
								'Message: ' + msg,
								'URL: ' + url,
								'Line: ' + lineNo,
								'Column: ' + columnNo,
								'Error object: ' + JSON.stringify(error)
							].join(' - ');
						
							$("<p/>").css("color", "red").css("font-weight", "bold").text(message).appendTo(window.$log);
						};
						break;
				}
			});					
		}		
	}
	
	var monthes = {
        1: "январь",
        2: "февраль",
        3: "март",
        4: "апрель",
        5: "май",
        6: "июнь",
        7: "июль",
        8: "август",
        9: "сентябрь",
        10: "октябрь",
        11: "ноябрь",
        12: "декабрь"
	};
	
    var defaultExtent = new Extent({
		"xmin": -1490063.75641351290,
		"ymin": 4080779.9893743116,
		"xmax": 2167808.142663619,
		"ymax": 6726618.614384895,
		"spatialReference":
		{
			"wkid": 32642
		}
	});
	
	function hideHover() {//скрыть подсказку
		$("#mouseoverMapHint").text(" ");
        //DijitPopup.close(dialog);
    }
	
	function getSymbol(color) {//символ который будет отрисован
        var outline = new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255, 255, 255]), 1);
        var symbol = new SimpleFillSymbol();
        symbol.setColor(new Color(color));
        symbol.setOutline(outline);
        return symbol;
    }

    function showHover(evt) {
        //показать всплывающую подсказку
        var obj = evt.graphic;
        if (obj) {
            var content = "";
            if (obj.attributes) {
                content = obj.attributes.NAME;
				
				if (obj.attributes.AREA_NAME && obj.attributes.AREA_NAME.length) {
					content += ", " + obj.attributes.AREA_NAME;
				}
            }
			
			$("#mouseoverMapHint").text(content);

            // dialog.setContent(content);

            // domStyle.set(dialog.domNode, "opacity", 0.85);
			
            // DijitPopup.open({
                // popup: dialog,
                // x: evt.pageX,
                // y: evt.pageY
            // });
        }
    };

    var SERVER_NAME = 'infopublic.pravstat.kz';

	// Объекты	
	var regions = [], 
		clusters = {}, 
		crimes = {},
		oblasts = [],
		
		selectedCrimeId = -1,
		selectedClusterId = -1,
		selectedCluster = null,
		selectedRegionCode = -1,
		selectedRegion = null,
		selectedPeriodEnd = null,
		selectedPeriodBegin = null,
		regionsToDisplay = [],
		selectedMarkerId = 1,
		
		dashboard8data = null,
		dashboard679data = null,
		chartData = null,
		
		mode = "map";
	
	// Геттеры / сеттеры	
	function setMode(value) {
		mode = value;
		
		switch (mode) {
			case "map":
				$("#map_item").show();
				$("#chart").hide();
				break;
				
			case "chart":
				$("#map_item").hide();
				$("#chart").show();
				jQuery(window).trigger('resize'); // чтобы D3 и C3 правильно установили размер графиков					
				break;
		}
	}
	
	function getSelectedMarker() {
		return selectedMarkerId;
	}
	
	function setSelectedMarker(markerId) {
		selectedMarkerId = markerId;
		$("#marker-1-choose, #marker-2-choose, #marker-3-choose").show();
		$("#marker-" + markerId + "-choose").hide();
	}
	
	function getSelectedRegionCode() {
		return selectedRegionCode;
	}
	
	function setSelectedRegionCode(regionCode) {
		if (regionCode == selectedRegionCode)
			return;
		
		console.log("Set: regionCode = " + regionCode);
		selectedRegionCode = regionCode;
				
		if (regionCode == 0) {
			selectedRegion = null;
			map.graphics.clear();
            map.graphics.redraw();	
			$("#dropdownRegion").css("font-weight", "300");
		} else {
			var foundRegions = regions.filter(function(region) { return region.attributes.CODE == regionCode; });
			if (!foundRegions.length) {
				console.error("При выборе региона регион " + regionCode + " не был найден в списке Аркгис, беда.");
				return;
			}
			
			selectedRegion = foundRegions[0];
			
			$("#map-pin").attr("href", ias3url + "?region=" + selectedRegion.attributes.CODE);
			
			var selectedRegionsOblasts = oblasts.filter(function(o) { return o.attributes.CODE == regionCode.substr(0, 4); });
			if (!selectedRegionsOblasts.length) {
				console.error("При выборе региона " + regionCode + " область региона (" + regionCode.substr(0, 4) + ") не была найден в списке Аркгис, беда.");
				return;
			}
			var selectedRegionsOblast = selectedRegionsOblasts[0];
			
			var selectedRegionCluster = getClusterIdOfSelectedRegion();
			if (selectedRegionCluster != getSelectedClusterId())
			{
				//$("#rangeCluster").val(selectedRegionCluster);
				setSelectedClusterId(selectedRegionCluster);
			}		

			if (selectedRegion.geometry && selectedRegionsOblast.geometry) {
				var highlightGraphic = new Graphic(selectedRegion.geometry, new SimpleLineSymbol(
					SimpleLineSymbol.STYLE_SOLID,
					new Color([255, 128, 0]), 
					3
				));
				var highlightGraphicOblast = new Graphic(selectedRegionsOblast.geometry, new SimpleLineSymbol(
					SimpleLineSymbol.STYLE_SOLID,
					new Color([100, 100, 100]),
					2
				));
				
				map.centerAndZoom(selectedRegion.geometry.getCentroid(), 2);
				
				map.graphics.clear();
				map.graphics.add(highlightGraphicOblast);
				map.graphics.add(highlightGraphic);
				map.graphics.redraw();			
			} else {
				map.graphics.clear();
				map.graphics.redraw();	
			}
			
			$("#dropdownRegion").css("font-weight", "600");
		}		
	}
	
	function getClusterIdOfSelectedRegion() {
		var selectedRegionCode = getSelectedRegionCode();
		for (var key in clusters) {
			var regionBelongsToThisCluster = clusters[key].list.some(function(r) { 
				return r.reg_code == selectedRegionCode;
			});
						
			if (regionBelongsToThisCluster) {
				return key;
			}			
		}
		
		return 0;
	}
	
	function getSelectedCrimeId() {
		return selectedCrimeId;
	}
	
	function setSelectedCrimeId(crimeId) {
		console.log("Set: crimeId = " + crimeId);
		selectedCrimeId = crimeId;
	}
	
	function getSelectedPeriodDateEnd() {
		return selectedPeriodEnd;
	}
	
	function getSelectedPeriodDateBegin() {
		return selectedPeriodBegin;
	}
	
	function setSelectedPeriod(dateBegin, dateEnd) {
		selectedPeriodBegin = dateBegin;
		selectedPeriodEnd = dateEnd;
		
		var periodBegin = getSelectedPeriodDateBegin().toLocaleString("dd.MM.yyyy");
		var periodEnd = getSelectedPeriodDateEnd().toLocaleString("dd.MM.yyyy");
		//$("#period-info").text("с " + periodBegin + " по " + periodEnd);
	}
	
	function getSelectedClusterId() {
		return selectedClusterId;
	}
	
	function setSelectedClusterId(clusterId) {
		if (clusterId == selectedClusterId)
			return;
		
		console.log("Set: clusterId = " + clusterId);
		selectedClusterId = clusterId;
		
		if (selectedClusterId == 0) {
			selectedCluster = null;
			
			regionsToDisplay = regions;	
			$("#dropdownCluster").val(0);			
			setSelectedRegionCode(0);
		} else {
			selectedCluster = clusters[clusterId];
			regionsToDisplay = regions.filter(function(region) {
				return selectedCluster.list.some(function(r) { return r.reg_code == region.attributes.CODE; });
			});			
					
			var selectedRegionCode = getSelectedRegionCode();
			if (!selectedCluster.list.some(function(r) { return r.reg_code == selectedRegionCode; } )) {
				var newSelectedRegion = selectedCluster.list[0].reg_code;
				$("#dropdownRegion").val(newSelectedRegion);
				setSelectedRegionCode(newSelectedRegion);
			}
		}				
	}
	
	// Загрузчики
	function loadDashboard8() {		
		return $.get("http://service.pravstat.kz/PiProducer/ias/map?clstr=" + getSelectedClusterId() + "&prt_date=" + getSelectedPeriodDateEnd() + "&crime=" + getSelectedCrimeId() + "&marker=" + getSelectedMarker()).done(function(data) {
			console.log("http://service.pravstat.kz/PiProducer/ias/map?clstr=" + getSelectedClusterId() + "&prt_date=" + getSelectedPeriodDateEnd() + "&crime=" + getSelectedCrimeId() + "&marker=" + getSelectedMarker());
			console.log("AJAX: dashboard8 OK");
			dashboard8data = data;			
			console.log('dashboard8data - '+JSON.stringify(dashboard8data));
		});
	}
	
	function loadDashboard679() {
		return $.get("http://service.pravstat.kz/PiProducer/ias/dashboard?reg=" + getSelectedRegionCode() + "&prt_date=" + getSelectedPeriodDateEnd() + "&crime=" + getSelectedCrimeId()).done(function(data) {
			console.log("AJAX: dashboard679 OK");			
			dashboard679data = data;
			console.log('dashboard679data - '+JSON.stringify(dashboard679data));
		});
	}
	
	function loadChart() {		
		return $.get("http://service.pravstat.kz/PiProducer/ias/chart" + getSelectedMarker() + "?reg=" + getSelectedRegionCode() + "&crime=" + getSelectedCrimeId())
			.done(function(data) {
				console.log("AJAX: chart OK");
				chartData = data;
			});
	}
	
	// Отрисовка		
	function renderChart() {
		console.log("Render: RenderChart begin.");
		return new Promise(function(resolve, reject) {		
			$("#line-chart").empty();
			$(".line-chart-title").hide();
			$(".pie-chart-title").hide();
			$("#pie-chart-1").empty();
			$("#pie-chart-2").empty();			
			$("#hint-line-chart-with-green").hide();
			$("#mouseoverChartHint").hide();
		
			if (getSelectedRegionCode() === 0)
			{			
				resolve();
				return;
			}
			
			if (!chartData || !chartData.list.length) {
				reject();
				return;
			}
			
			$(".line-chart-title").hide();
			$(".pie-chart-title").hide();
			$("#pie-chart-1").empty();
			$("#pie-chart-2").empty();			
			$("#hint-line-chart-with-green").hide();
			
			var marker = getSelectedMarker();
			if (marker != 3) {				
				chartData.list = chartData.list
					.filter(function(x) {
						return x.y == selectedPeriodEnd.split(".")[2];
					})
					.sort(function(a, b) {
						return a.m - b.m;
					});
			
				// Line chart				
				lineChart = new Chart2D(dom.byId("line-chart"));
				lineChart.setTheme(chThemesThreeD);
				lineChart.addAxis("x", {
					fixLower: "minor", 
					fixUpper: "minor", 
					natural: true,
					dropLabels: false,
					max: chartData.list.length, //число меток
					includeZero: false,
					font: "normal normal bold 8pt Arial",
					rotation: 315,
					labelFunc: function(text, value, precision) {
						//console.log(value + " "+JSON.stringify(chartData.list[value-1]));
						return ("0" + chartData.list[value - 1].m).slice(-2) + "." + chartData.list[value - 1].y;
					},
					title: "Месяцы",
					titleOrientation: "away",
					majorTick: {
						width: 1.0
					},
					minorTick: {
						width: 1.0
					}
				});

				var combined = { };
				
				combined.values = chartData.list.map(function(x) { 
					x.data = marker === 1 ? +(x.count_1).toFixed(2) : +(x.grown).toFixed(2); 
					return x.data;
				});
				combined.chartHint =  chartData.list.map(function(x) {		
                    var hint = '';		
                    if (marker === 1) {		
                        hint = "Число преступлений: " + (x.crime_count).toFixed(2) + ', то есть ' + (x.count_1).toFixed(2)+' на 10 тыс. населения ';		
                    } else if (marker === 2) {		
                        hint = (x.grown).toFixed(2) + ', то есть прирост показателя в рассматриваемом месяце: '+(x.count_first).toFixed(2)+', при среднем годовом приросте: '+(x.grown_1).toFixed(2);		
                    }		
                    return hint;		
                });		
				combined.high1 = chartData.list.map(function(x) { return x.b_1; });
				combined.high2 = chartData.list.map(function(x) { return x.b_2; });
				combined.high3 = chartData.list.map(function(x) { return x.b_3; });
				combined.low1 = chartData.list.map(function(x) { return x.b__1; });
				combined.low2 = chartData.list.map(function(x) { return x.b__2; });
				combined.low3 = chartData.list.map(function(x) { return x.b__3; });
				
				var max = Math.max(Math.max.apply(Math, combined.values), Math.max.apply(Math, combined.high3));
				var min = Math.min(Math.min.apply(Math, combined.values), Math.min.apply(Math, combined.low3));
				
				var mean = (max + min) / 2; // вычисляем среднее значение минимума и максимума (середину оси oY)
				
				max += (max - mean) * 0.1; // сдвигаем верхнюю границу графика на 10% вверх
				min -= (mean - min) * 0.1; // сдвигаем верхнюю границу графика на 10% вниз
				
				if (marker === 1) {
					min = 0; // У графика 1 нет отрицательных значений
				}
				
				var selectedValue = chartData.list.filter(function(x) {
					return x.y == selectedPeriodEnd.split(".")[2] && x.m == selectedPeriodEnd.split(".")[1];
				})[0];
				
				combined.max = chartData.list.map(function(x) { return max; });
				combined.min = chartData.list.map(function(x) { return min; });
				
				lineChart.addPlot("lines", { //наименование
					type: "Lines", //тип - линии
					labels: true, //показывать надписи
					precision: 0, //число цифр после запятой
					markers: true,  //показывать символы
					styleFunc: function(value) {
						if (selectedValue.data == value) {
							return {
								stroke: {
									color: "black",
									width: 2
								},
								fill: "rgb(220, 110, 0)"
							};
						}
						
						return {};
					}
				});
				
				var magnify = new Magnify(lineChart, "lines");
							
				lineChart.addSeries(selectedRegion.attributes.NAME,
					combined.values,
					{   
						stroke: 
						{
							color: "black",
							width: 2
						},
						fill: "white",
						plot: "lines"
					});
				
				lineChart.addPlot("areas", { //наименование
					type: "Areas", //тип - линии
					labels: true //показывать надписи						
				});
			
				lineChart.addSeries("dark-green",
					combined.low3,
					{   
						stroke: {
							color: marker == 1 ? "rgb(40, 255, 40)" : "rgb(255, 40, 40)",
							width: 0.0
						},
						fill: marker == 1 ? "rgb(40, 255, 40)" : "rgb(255, 40, 40)",
						plot: "areas"
					});		
				
				lineChart.addSeries("green",
					combined.low2,
					{   
						stroke: {
							color: marker == 1 ? "rgb(75, 255, 75)" : "rgb(255, 120, 120)",
							width: 0.0
						},
						fill: marker == 1 ? "rgb(75, 255, 75)" : "rgb(255, 120, 120)",
						plot: "areas"
					});				

				lineChart.addSeries("light-green",
					combined.low1,
					{   
						stroke: {
							color: marker == 1 ? "rgb(150, 255, 150)" : "rgb(255, 160, 160)",
							width: 0.0
						},
						fill: marker == 1 ? "rgb(150, 255, 150)" : "rgb(255, 160, 160)",
						plot: "areas"
					});
				
				lineChart.addSeries("normal",
					combined.high1,
					{   
						stroke: {
							color: "rgb(127, 193, 253)",
							width: 0.0
						},
						fill: "rgb(127, 193, 253)",
						plot: "areas"
					});
				
				
				lineChart.addSeries("light-red",
					combined.high2,
					{   
						stroke: {
							color: "rgb(255, 160, 160)",
							width: 0.0
						},
						fill: "rgb(255, 160, 160)",
						plot: "areas"
					});
					
				lineChart.addSeries("red",
					combined.high3,
					{   
						stroke: {
							color: "rgb(255, 120, 120)",
							width: 0.0
						},
						fill: "rgb(255, 120, 120)",
						plot: "areas"
					});
					
				lineChart.addSeries("dark-red",
					combined.max,
					{   
						stroke: {
							color: "rgb(255, 40, 40)",
							width: 0.0
						},
						fill: "rgb(255, 40, 40)",
						plot: "areas"
					});		

				lineChart.connectToPlot("lines", function(evt) {
					if (evt.element !== "marker")
						return;
					
					switch (evt.type) {
						case "onmouseover":
							$("#mouseoverChartHint").text(combined.chartHint[evt.index]);
							break;
						case "onmouseout":
							$("#mouseoverChartHint").text("");
							break;
					} 
				});		
					
				var titles = ["", "Число преступлений", "Приросты показателя преступности"];
				lineChart.addAxis("y", {
					vertical: true,
					fixLower: "minor",
					fixUpper: "minor",
					max: max,
					min: min
				});
				
				lineChart.render();
				
				$("#line-chart svg").attr("height", 360).attr("width", 550); // Так себе решение, но lineChart.resize() не работает. 					
				lineChart.fullRender();
				
				$("#mouseoverChartHint").show();
				$(".line-chart-title").show();
				
				if (marker == 1) {
					$(".line-chart-title").text("Число преступлений на 10 тыс. населения");
				} else {
					$(".line-chart-title").text("Отклонение от годового среднего показателя");
				}
				$("#hint-line-chart-with-green").show();
				console.log("Render: line-chart отрисован");
			} else {
				// Пайчарт
				var regionColumns = [],
					clusterColumns = [];
					
				var selectedCrime = getSelectedCrimeId();
				
				if (selectedCrime >= 1 && selectedCrime <= 5) {
					$("#pie-chart-1").parent().removeClass("col-xs-offset-3");
					$("#pie-chart-2").parent().show();
					
					var specificCrimes = [6, 7, 13, 14, 15, 16, 17, 18, 78, 100];
					specificCrimes.forEach(function(crimeId)
					{					
						var chartDataRow = chartData.list.filter(function(x) { 
							return x.c15 == selectedCrime && x.c6 == crimeId; 
						});
						
						var crimeName = crimeId === 100 ? "Иные" : crimes[crimeId]; 

						if (chartDataRow.length) {
							regionColumns.push([crimeName, chartDataRow[0].c7]);
							clusterColumns.push([crimeName, chartDataRow[0].c13]);
						} else {
							regionColumns.push([crimeName, 0]);
							clusterColumns.push([crimeName, 0]);
						}
					});
					
					var chart1 = c3.generate({
						size: {
							width: 270
						},
						pie: {
							expand: false
						},
						data: {
							columns: regionColumns,
							type: "pie"
						},
						bindto: "#pie-chart-1",
						color: {
							pattern: ['#1f77b4', '#aec7e8', '#ff7f0e', '#ffbb78', '#2ca02c', '#98df8a', '#d62728', '#ff9896', '#9467bd', '#c5b0d5', '#8c564b', '#c49c94', '#e377c2', '#f7b6d2', '#7f7f7f', '#c7c7c7', '#bcbd22', '#dbdb8d', '#17becf', '#9edae5']
						}
					});
				
					var chart2 = c3.generate({
						size: {
							width: 270
						},
						data: {
							columns: clusterColumns,
							type: "pie"
						},
						pie: {
							expand: false
						},
						bindto: "#pie-chart-2",
						color: {
							pattern: ['#1f77b4', '#aec7e8', '#ff7f0e', '#ffbb78', '#2ca02c', '#98df8a', '#d62728', '#ff9896', '#9467bd', '#c5b0d5', '#8c564b', '#c49c94', '#e377c2', '#f7b6d2', '#7f7f7f', '#c7c7c7', '#bcbd22', '#dbdb8d', '#17becf', '#9edae5']
						}
					});	
				} else {
					$("#pie-chart-1").parent().addClass("col-xs-offset-3");
					$("#pie-chart-2").parent().hide();
					
					chartData.list
						.filter(function(x) {
							return x.c15 == selectedCrime;
						})
						.forEach(function(rec) {
							regionColumns.push([rec.c14, +rec.c6]);
						});
						
					var chart1 = c3.generate({
						size: {
							width: 270,
							height: 400
						},
						pie: {
							expand: false
						},
						data: {
							columns: regionColumns,
							type: "pie"
						},
						bindto: "#pie-chart-1",
						color: {
							pattern: ['#1f77b4', '#aec7e8', '#ff7f0e', '#ffbb78', '#2ca02c', '#98df8a', '#d62728', '#ff9896', '#9467bd', '#c5b0d5', '#8c564b', '#c49c94', '#e377c2', '#f7b6d2', '#7f7f7f', '#c7c7c7', '#bcbd22', '#dbdb8d', '#17becf', '#9edae5']
						}
					});
				}											
				
				$(".pie-chart-title").show();
				console.log("Render: pie-chart отрисован");
			}
			
			resolve();
		}).then(function() {
			console.log("Render: RenderChart complete.");
		});
	}
	
	function renderDashboard679() {
		console.log("Render: RenderDashboard679 begin.");
		return new Promise(function(resolve, reject) {
			if (getSelectedRegionCode() === 0) {
				$("#dashboard").hide();
				$("#marker-description").hide();
				resolve();
				return;
			}
			
			$("#dashboard").show();
			$("#marker-description").show();
			
			//$("#selected-region-name").text($("#dropdownRegion option:selected").text());
			
			$("#last-year-growth")
				.text(dashboard679data.t7_y1_y2 > 0 ? "+" + dashboard679data.t7_y1_y2.toFixed(1) : dashboard679data.t7_y1_y2.toFixed(1))
				.css("color", dashboard679data.t7_y1_y2 > 0 ? "rgb(247, 69, 59)" : "rgb(48, 151, 32)");
						
			// show speedometers for indicators 1, 2: 
			showSpeedometer(1, dashboard679data.t2_color);
			showSpeedometer(2, dashboard679data.t4_color);
			
			// show circle for indicator 3:			
			$("#chart_3").empty().html("<svg xmlns='http://www.w3.org/2000/svg' width='80' height='80'><circle cx='40' cy='40' stroke='black' stroke-width='0.3' r='37' fill='" + (dashboard679data.t5_color == 3 ? "rgb(211,211,44)" : defaultColors1[2]) + "'></circle></svg>");
			
			var real_color;
			
			var selectedDateEnd = getSelectedPeriodDateEnd();
			var selectedMonth = +selectedDateEnd.split(".")[1];
			var selectedYear = +selectedDateEnd.split(".")[2];
			
			switch (getSelectedMarker())
			{
				case 1:
					//$("#rank_value").text(dashboard679data["t2_rank"].toString().split(".")[0]);
					//$("#rank_max").text(dashboard679data["t2_max_score"].toFixed(0));				
					
					$(".tab-content").show();
					
					var countOfCrimesList = chartData.list.filter(function(x) {
						return x.m == selectedMonth && x.y == selectedYear;
					});
					
					if (countOfCrimesList.length !== 1) {
						$("#raw_value").text("N/A");
					} else {
						//console.log('StaMap:js:793'+JSON.stringify(countOfCrimesList[0].count_1.toFixed(1)));
						$("#raw_value").text(countOfCrimesList[0].count_1.toFixed(1));
						$("#dashboard-1").tooltipster("enable").tooltipster("content", "Всего преступлений: " + countOfCrimesList[0].crime_count.toFixed(2));
					}
					
					$("#deviation-from-average").text(dashboard679data["t2_otklonenie"] >= 0 
						? "+" + dashboard679data["t2_otklonenie"].toFixed(1) + "%"
						: dashboard679data["t2_otklonenie"].toFixed(1) + "%");
						
					real_color = dashboard679data["t2_color"];
					$("#raw_value").show();
					$("#rank_value, #rank_max").hide();
					$("#points_desc").html("Число преступлений на 10 тыс. населения");
					$("#medium_points_desc").html("Отклонение от среднего в группе");
					$("#dashboard-8-title").text("Рейтинг преступности за весь год").tooltipster("enable").tooltipster("content", "Баллы аномальности каждого месяца суммируются и составляется годовой рейтинг районов");
					$("#last-year-container").show();
					$("#marker-description-content").html("Сейчас в таблице выше и на карте цветами выделены районы одной группы (сравнимые) с выбранным. Окраска выполнена по результатам работы индикатора 1 на заданный период времени:<br/>- Анализ уровня преступности помогает находить высокие (красные) / низкие (зеленые) показатели преступности районов и проставлять соответствующие баллы аномальности в каждом месяце. Суммируя их, составляется рейтинг по баллам за год в таблице. <br/>Для просмотра динамики показателя во времени и подсчета баллов, переключитесь под картой на режим анализа в графике.");
					$("#pie-chart").hide();
					break;
				case 2:
					//$("#rank_value").text(dashboard679data["t4_rank"].toString().split(".")[0]);
					//$("#rank_max").text(dashboard679data["t4_max_score"].toFixed(0));				
					
					$(".tab-content").show();
										
					$("#deviation-from-average").text(dashboard679data["t4_otklonenie"] >= 0 
						? "+" + dashboard679data["t4_otklonenie"].toFixed(1) + "%"
						: dashboard679data["t4_otklonenie"].toFixed(1) + "%");
						
					var grownList = chartData.list.filter(function(x) {
						return x.m == selectedMonth && x.y == selectedYear;
					});
					
					if (grownList.length !== 1) {
						$("#raw_value").text("N/A");
					} else {
						$("#raw_value").text(grownList[0].grown.toFixed(1));
						$("#dashboard-1").tooltipster("enable").tooltipster("content", 'То есть прирост показателя в рассматриваемом месяце: ' + (grownList[0].count_first).toFixed(2)+ ', при среднем годовом приросте: '+(grownList[0].grown_1).toFixed(2));		
					}						
						
					real_color = dashboard679data["t4_color"];
					$("#raw_value").show();
					$("#rank_value, #rank_max").hide();
					$("#points_desc").html("Показатель неустойчивости в динамике");
					$("#medium_points_desc").html("Отклонение показателя от среднего в группе");
					$("#dashboard-8-title").text("Рейтинг по баллам аномальности за год").tooltipster("enable").tooltipster("content", "Баллы аномальности каждого месяца суммируются и составляется годовой рейтинг районов");
					$("#last-year-container").show();
					$("#marker-description-content").html("Анализ уровня неустойчивости в динамике помогает проследить аномальные скачки выбранного вида преступности. <br/>Формула: (Прирост преступности в заданном месяце) / (Средний годовой прирост преступлености).<br/>Для просмотра динамики во времени, переключитесь под картой на режим анализа в графике.");
					$("#pie-chart").hide();
					break;
				case 3: 
					$(".tab-content").hide();
					$("#pie-chart").show();
					$("#marker-description-content").html("Данный анализ помогает находить районы (подсвечены желтым цветом), в которых замечена специфичная структура преступлений на фоне стандартного распределения преступности в группе. <br/><a href='http://ucanalytics.com/blogs/population-stability-index-psi-banking-case-study/'>Ссылка на методологию расчета показателя неустойчивости в структуре (индекса PSI).</a><br/>Данный показатель рассчитывается за весь год.")
					break;
			}
			
			$(".medium_points, #medium_points_digit").toggleClass("blue", real_color == 1).toggleClass("lightred", real_color == 2);
			
			console.log("Render: дэшбоард отрисован");
			resolve();
		}).then(function() {
			console.log("Render: RenderDashboard679 complete.");
		});
	}
	
	$("#dashboard-8 tbody").on("click", "tr", function() {
		setSelectedRegionCode($(this).data("code"));
		$("#dropdownRegion").val($(this).data("code"));
		searchRegionByCode($(this).data("code"));
		renderAll();
	});
	
    function renderMap() { 	
		console.log("Render: RenderMap begin.");
		return new Promise(function(resolve, reject) {
			if (getSelectedClusterId() === 0) {
				map.canvasRegion.clear();	
			
				$("#dashboard-8 tbody").empty();
				regions.forEach(function(region) {
					region.attributes.color = defaultColors[0];
					
					var gr = new Graphic(region.geometry, getSymbol(region.attributes.color), region.attributes);
					map.canvasRegion.add(gr);
				});

				map.canvasRegion.redraw();
				map.canvasText.redraw();		

				resolve();
				console.log("Render: карта отрисована без подсветки, так как кластер не выбран");
			} else {
				// Рисуем дэшборд 8
				var $dashboardBody = $("#dashboard-8 tbody").empty();
				
				var marker = getSelectedMarker();				
				dashboard8data.list.forEach(function(x) {					
					if (marker == 3) 
						x.real_score = +x.score.toFixed(3);
					else
					{
						x.real_score = +x.rank.toString().split(".")[1] || "0";
					}					
				});
				
				var rating = dashboard8data.list.sort(function(a, b) {
					return b.real_score - a.real_score;
				});
					
				rating.forEach(function(x, i) {
					x.real_rank = (i + 1).toString(); 
				});	
				
				var selectedRowIndex = 0;
				
				for (var i = 0; i < rating.length; i++) {
					var bestRegion = rating[i];
					
					var $row = $("<tr/>").appendTo($dashboardBody);
					var $td1 = $("<td/>").addClass("rate_1").text("#" + bestRegion.real_rank).appendTo($row);
					var $td2 = $("<td/>").addClass("rate_2").text(bestRegion.reg_name).appendTo($row);
					var $td3 = $("<td/>").addClass("rate_green").text(bestRegion.real_score).appendTo($row);
					$row.data("code", bestRegion.reg_code);
					
					if (bestRegion.reg_code == selectedRegionCode) {
						$td1.css("font-weight", "600");
						$td2.css("font-weight", "600");
						$td3.css("font-weight", "600");
						selectedRowIndex = i;						
					}
				}
				
				setTimeout(function() {
					$("#dashboard-8 tbody").animate({
						"scrollTop": selectedRowIndex * 33 - 66
					});
					searchRegionByCode(selectedRegionCode);
				}, 250);
			
				// Рисуем карту
				map.canvasRegion.clear();	
			
				var marker = getSelectedMarker();
			
				regions.forEach(function(region) {
					var color;
					var dashboard8records = dashboard8data.list.filter(function(x) { return x.reg_code === region.attributes.CODE; });
					
					if (marker == 3) {		
						color = defaultColors1[0];
						
						if (dashboard8records.length) {
							if (dashboard8records[dashboard8records.length - 1].color == 3) {
								color = "rgb(211,211,44)";
							} else {
								color = defaultColors1[2];
							}						
						}					
					} else {
						color = defaultColors[0];						
						if (dashboard8records.length) {
							color = defaultColors[dashboard8records[dashboard8records.length - 1].color];
						}
					}
					
					region.attributes.color = color;
										
					var gr = new Graphic(region.geometry, getSymbol(region.attributes.color), region.attributes);
					map.canvasRegion.add(gr);
				});

				map.canvasRegion.redraw();
				map.canvasText.redraw();		

				resolve();
				console.log("Render: карта отрисована с подсветкой");					
			}			
		}).then(function() {
			console.log("Render: RenderMap complete.");
		});
    }   
	
	var neverAnalyzed = true;
	
	function renderAll() {	
		$("#analyze").prop("disabled", true);
	
		// У нас нет кнопки Режим просмотра, когда не выбран район (так как нет графика) или когда маркер 3
		if (selectedClusterId == 0 || selectedMarkerId == 3) { 
			setMode("map");
			$(".map-chart-switch").hide();
		} else {
			$(".map-chart-switch").show();
		}		
	
		$("#dropdownCluster").val(selectedClusterId);
	
		console.log("Render: RenderAll begin.");
		$("body").addClass("loading");
		return new Promise(function(resolve, reject) {
			$("html, body").animate({
				"scrollTop": 200
			}, 200, "swing", function() {
				if (this.nodeName.toLowerCase() !== "body")
					return;
				
				$.when(loadDashboard679(), loadDashboard8(), loadChart())
					.done(function() {
						Promise.all([
							renderDashboard679(),
							renderMap(),
							renderChart()
						]).then(function() {
							resolve();
						});
					})
					.fail(function() {
						reject();
					});				
			});
		}).then(function() {
			console.log("Render: RenderAll complete.");
			$("body").removeClass("loading");
			$("#analyze").prop("disabled", false);
			$(window).trigger("resize"); // для D3
		});		
	}
	

	//Region Search
	function searchRegionByCode(code){
		var html =  '', value = ''
		$('#region-search-prompt .hidden option').each(function(){
			var $t = $(this).clone();
			if (code == $(this).attr('value')) {
				$('#region-search input').val($t.text());
				$t.addClass('bold');
			}
			html+=$t.outerHTML();
			$('#region-search-prompt .body').html(html);
		});
	}

	jQuery.fn.outerHTML = function(s) {
	    return s
	        ? this.before(s).remove()
	        : jQuery("<p>").append(this.eq(0).clone()).html();
	};

	$('#region-search input').keyup(function(){
		var value = jQuery.trim($(this).val()).toLowerCase();
		var html = '';
		$('#region-search-prompt .hidden option').each(function(){
			var text = jQuery.trim($(this).text()).toLowerCase();
			if (text.indexOf(value)!=-1 || value == '') {
				html+=$(this).outerHTML();
			}
			$('#region-search-prompt .body').html(html);
		});
	});

	$('#region-search input').focus(function(){
		$('#region-search-prompt').show();
	});

	$(document).on('click','#region-search-prompt .body option',function(){					
		$('#region-search-prompt .body option').removeClass('bold');
		$(this).addClass('bold');
		$('#region-search input').val($(this).text());					
		
		// if (selectedClusterId == 0) {
			// setSelectedRegionCode($(this).val());		
			// setSelectedClusterId(15);
			// renderAll();
		// } else {
			setSelectedRegionCode($(this).val());		
		//}		
	});

	$('#region-search input').focusout(function(){
		setTimeout(function() { 
			$('#region-search-prompt').hide();
		}, 300);
		
	});				
	//endOf RegionSearch

	$("#dropdownRegion").change(function(e) {
		e.preventDefault();
		
		setSelectedRegionCode($(this).val());
	});
	
	$("#dropdownCluster").change(function(e) {
		e.preventDefault();
		
		setSelectedClusterId($(this).val());				
		
		if (neverAnalyzed) {
			$("#hint-begin").remove();

			neverAnalyzed = true;
		}
		//Districts, Regions	
		
		for (var i = 1; i <= clustersCount; i++) // TODO: переделать на работу со списком из API, а не сверку. Не было времени
		{
			clusters[i].list.forEach(function(apiRegion) {
				if (!regions.some(function(arcgisRegion) {
					return arcgisRegion.attributes.CODE == apiRegion.reg_code;
				})) 
				{
					console.debug("Район " + apiRegion.reg_code + " (" + apiRegion.reg_name + ") не был найден в ArcGis, добавляем ручками :(");
					regions.push({
						attributes: {
							CODE: apiRegion.reg_code,
							NAME: apiRegion.reg_name,
							AREA_NAME: ""
						},
						geometry: null
					});
				}
			});
		}		
		//endOf Districts, Regions

		renderAll();		
	});
	
	$("#dropdownCrime").change(function(e) {
		e.preventDefault();
		
		setSelectedCrimeId($(this).val());
	});
	
	$("#dropdownPeriod").change(function(e) {
		e.preventDefault();
		
		setSelectedPeriod($("#dropdownPeriod option:selected").data("begin"), $("#dropdownPeriod option:selected").data("end"));
	});	
	
	$("#map-pin").click(function(e) {
		if (getSelectedRegionCode() == 0) {
			e.preventDefault();
		}
	});
	
	$("#speedometer1").click(function(e) {	
		if (!$(this).parent().hasClass("active")) { 
			setSelectedMarker(1);
			renderAll();
		}
	});
	
	$("#speedometer2").click(function(e) {		
		if (!$(this).parent().hasClass("active")) { 
			setSelectedMarker(2);
			renderAll();
		}
	});
	
	$("#speedometer3").click(function(e) {	
		if (!$(this).parent().hasClass("active")) { 	
			setSelectedMarker(3);
			renderAll();
		}
	});
	
	$("#analyze").click(function(e) {	
		e.preventDefault();		
		
		if (neverAnalyzed) {
			$("#hint-begin").remove();
			neverAnalyzed = true;
		}
		renderAll();
	});
	
	function resetAll() {
		$("#dropdownCluster").val(0);
		$("#dropdownCrime option:first").prop("selected", true);
		$("#dropdownRegion").val(0);
		$('#region-search input').val('');
		$("#dropdownPeriod option:last").prop("selected", true);
		
		$(".nav-tabs li").removeClass("active");
		$(".nav-tabs li").first().addClass("active");
		$("#dashboard-8 tbody").empty();
		$("#line-chart").empty();
		$("#pie-chart-1").empty();
		$("#pie-chart-2").empty();
		
		setSelectedCrimeId($("#dropdownCrime").val());
		setSelectedPeriod($("#dropdownPeriod option:selected").data("begin"), $("#dropdownPeriod option:selected").data("end"));
		setSelectedClusterId($("#dropdownCluster").val());
		setSelectedMarker(1);
		setSelectedRegionCode(0);
		
		setMode("map");
		map.setExtent(defaultExtent);
	}
	
	$("#clear").click(function(e) {
		e.preventDefault();
		
		resetAll();
		renderAll();
	});
	
	$(".map-chart-switch").click(function(e) {
		e.preventDefault();
		
		$("html, body").animate({
			"scrollTop": 200
		}, 200, "swing", function() {		
			if (this.nodeName.toLowerCase() !== "body")
				return;
			
			setMode(mode === "map" ? "chart" : "map");
		});
	});
	



	function init() {
		var initPromises = [];
		
		// Всплывающая подсказка
		dialog = new TooltipDialog({
			id: "tooltipDialog",
			style: "position: absolute; width: 250px; font: normal normal normal 10pt Helvetica;z-index:100"
		});
		dialog.startup();
		
		// 1. Грузим карту из arcgis
		var loadMapPromise = new Promise(function(resolve, reject) {
			map = new Map("map", {
				extent: defaultExtent,
                logo: false
            });
            //базовый слой
            var tile = new ArcGISTiledMapServiceLayer('http://' + SERVER_NAME + ':8399/arcgis/rest/services/KZ_AREA_RU/MapServer', {id: 'kpsisu'});
            map.addLayer(tile);
			
			//map.setZoom(0);
			
            //слои на которых будем рисовать
            map.canvasRegion = new GraphicsLayer();
            map.addLayer(map.canvasRegion, 10);
            map.canvasText = new GraphicsLayer();
            map.addLayer(map.canvasText, 20);
            map.infoWindow.resize(245, 125);
            
            map.on("load", function () {//окончание загрузки карты							
				map.disableMapNavigation();  //запрет навигации
				map.enablePan(); // разрешаем двигать влево вправо
				domStyle.set(query("a.action.zoomTo")[0], "display", "none");
			
				map.canvasRegion.on("mouse-out", hideHover);
				map.canvasRegion.on("mouse-over", showHover);
				map.canvasText.on("mouse-over", showHover);
				
				map.canvasRegion.on("click", function(e) {
					$("#dropdownRegion").val(e.graphic.attributes.CODE);
					searchRegionByCode(e.graphic.attributes.CODE);
					setSelectedRegionCode(e.graphic.attributes.CODE);
					
					if (neverAnalyzed) {
						$("#hint-begin").remove();
						neverAnalyzed = true;
					}
					renderAll();
				});
				
				console.log("Загрузка: Карта из ArcGis загружена.");
			
				resolve();
			});		
		});
		initPromises.push(loadMapPromise);
				
		// 2. Загружаем районы из arcgis
		var loadRegionsPromise = new Promise(function(resolve, reject) {
			var queryTask = new QueryTask('http://' + SERVER_NAME + ':8399/arcgis/rest/services/KZ_AREA_RU/MapServer/8');
			var queryGIS = new qr();
			queryGIS.outFields = ["CODE", "NAME", "AREA_NAME"];			
			queryGIS.returnGeometry = true;
			queryGIS.outSpatialReference = map.spatialReference;
			queryGIS.orderByFields = ["CODE"];
			queryGIS.where = 'rownum > 0';
			queryTask.execute(queryGIS, function(res) {
				regions = res.features;
				console.log("Загрузка: Районы из ArcGis загружены.");
				resolve();
			}, reject);
		});
		initPromises.push(loadRegionsPromise); 


		// 3. Загружаем области из arcgis
		var loadRegionsPromise2 = new Promise(function(resolve, reject) {
			var queryTask = new QueryTask('http://' + SERVER_NAME + ':8399/arcgis/rest/services/KZ_AREA_RU/MapServer/9');
			var queryGIS = new qr();
			//queryGIS.outFields = ["CODE", "NAME", "AREA_NAME"];			
			queryGIS.returnGeometry = true;
			queryGIS.outSpatialReference = map.spatialReference;
			//queryGIS.orderByFields = ["CODE"];
			queryGIS.where = 'rownum > 0';
			queryTask.execute(queryGIS, function(res) {				
				oblasts = res.features;				
				console.log("Загрузка: Регионы из ArcGis загружены.");
				resolve();
			}, reject);
		});
		initPromises.push(loadRegionsPromise2);		
		
		// 4. Загружаем преступления из бэка
		var loadCrimes = new Promise(function(resolve, reject) {
			$.get("http://service.pravstat.kz/PiProducer/ias/crime").done(function(data) {
				$("#dropdownCrime").empty();
				data.list.forEach(function(x) {
					var crimeText = x.crime_id == 1 ? "Всего преступлений" : x.crime_name;
					$("<option/>").text(crimeText).attr("value", x.crime_id).appendTo("#dropdownCrime");
					crimes[x.crime_id] = x.crime_name;
				});
				
				console.log("Загрузка: Типы преступлений из ArcGis загружены.");
				resolve();
			});
		});
		initPromises.push(loadCrimes);
	
		// 5. Загружаем периоды из бэка
		var loadPeriods = new Promise(function(resolve, reject) {
			$.get("http://service.pravstat.kz/PiProducer/ias/period").done(function(data) {
				$("#dropdownPeriod").empty();
				
				data.list = data.list.sort(function(a, b) {
					var aSplit = a.date_end.split(".").map(function(x) { return +x; });
					var bSplit = b.date_end.split(".").map(function(x) { return +x; });
					
					if (aSplit[2] > bSplit[2])
						return 1;
					
					if (aSplit[2] < bSplit[2])
						return -1;
					
					if (aSplit[1] > bSplit[1])
						return 1;
					
					if (aSplit[1] < bSplit[1])
						return -1;
					
					if (aSplit[0] > bSplit[0])
						return 1;
					
					if (aSplit[0] < bSplit[0])
						return -1;
					
					return 0;
				});
				
				var currentYear = "",
					$currentOptGroup = null;
				data.list.forEach(function(x) {
					if (x.date_begin.split(".")[2] !== currentYear) {
						currentYear = x.date_begin.split(".")[2];
						$currentOptGroup = $("<optgroup/>").attr("label", currentYear).appendTo("#dropdownPeriod");
					}
					$("<option/>").text(x.date_begin + " - " + x.date_end).data("begin", x.date_begin).data("end", x.date_end).appendTo($currentOptGroup);
				});
				
				$("#dropdownPeriod option:first").prop("selected", true);
				console.log("Загрузка: Периоды из бэка загружены.");
				resolve();
			});
		});
		initPromises.push(loadPeriods);		
		
		// 6. Загружаем все кластеры из бэка
		var loadClustersPromise = new Promise(function(resolve, reject) {
			var clusterAjaxes = [];
			for (var i = 1; i <= clustersCount; i++)
			{
				(function(iLocal) {
					var ajax = $.get("http://service.pravstat.kz/PiProducer/ias/region?clstr=" + iLocal)
						.done(function(data) {
							
							console.log("Загрузка: Информация по кластеру " + iLocal + " из бэка загружена.");
							clusters[iLocal] = data;
						});
					clusterAjaxes.push(ajax);
				})(i);				
			}
			
			// Простите за мешанину deferred и промисов, не было времени писать красивый код
			$.when.apply($, clusterAjaxes).done(function() { 
				console.log("Загрузка: Информация по всем " + clustersCount + " кластерам загружена.");
				resolve(); 
			});
		});
		initPromises.push(loadClustersPromise);
		
		// Ждём, пока всё это загрузится и отрисовываем карту		
		Promise.all(initPromises).then(function() {
			// Сверяем кластеры
			for (var i = 1; i <= clustersCount; i++) // TODO: переделать на работу со списком из API, а не сверку. Не было времени
			{
				clusters[i].list.forEach(function(apiRegion) {
					if (!regions.some(function(arcgisRegion) {
						return arcgisRegion.attributes.CODE == apiRegion.reg_code;
					})) 
					{
						console.debug("Район " + apiRegion.reg_code + " (" + apiRegion.reg_name + ") не был найден в ArcGis, добавляем ручками :(");
						regions.push({
							attributes: {
								CODE: apiRegion.reg_code,
								NAME: apiRegion.reg_name,
								AREA_NAME: ""
							},
							geometry: null
						});
					}
				});
			}
			
			var $dropdownRegion = $("#dropdownRegion").empty();
			/*
			$("<option/>").attr("value", 0).attr("disabled", true).text("Поиск района").appendTo($dropdownRegion);
			regions.sort(function(a, b) {
				return a.attributes.NAME.localeCompare(b.attributes.NAME);
				return 0;
			}).forEach(function(reg) {
				var name = reg.attributes.NAME;
				if (reg.attributes.AREA_NAME.length)
					name += ", " + reg.attributes.AREA_NAME;
				$("<option/>").attr("value", reg.attributes.CODE).text(name).appendTo($dropdownRegion);				
				$('#region-search-prompt .body').append('<p class="block" value="'+reg.attributes.CODE+'">'+name+'</p>');				
			});			
			console.log("Загрузка: Вся информация загружена, начинаем отрисовывать карту.");
			*/
			resetAll();			
			renderAll().then(function() {
				$("body").removeClass("loading");
			});
		});
	}
	
    return {
        init: init
    };
});



$(document).ready(function(){
	
});