/**
 * Author: Шестаков П.Н.
 * Script: Вспомогательные функции
 */
define([
    "esri/Color",
    "analytics/Const"

], function (Color, Const) {
    var Util = function () {
    };

    Util.formatDate = function (mls, nls) { // дата в виде строки dd.mm.yyyy
        return Util.dateToStr(new Date(mls), nls);
    }

    Util.dateToStr = function (date, nls) {
        nls = nls || "EN";
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var day = date.getDate();
        if (day < 10) {
            day = '0' + day;
        }
        if (month < 10) {
            month = '0' + month;
        }
        if (nls == "EN") {
            return year + '-' + month + '-' + day;
        } else {
            return day + '.' + month + '.' + year;
        }
    }

    Util.findInGraphics = function (arr, attribute, val) {//поиск в массиве Graphic по атрибуту
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].attributes[attribute] == val) {
                return i;
            }
        }
        return -1;
    }

    Util.nvl = function (value, defaultVal) {//замена неопределенного значения
        var v = defaultVal;
        if (value) {
            v = value;
        }
        return v;
    }

    Util.createGradient = function (colorFrom, colorTo, breakCount, opacity) {//создание градиента
        var colorArr = [];
        var deltaR = Math.round((colorTo.r - colorFrom.r) / breakCount);
        var deltaG = Math.round((colorTo.g - colorFrom.g) / breakCount);
        var deltaB = Math.round((colorTo.b - colorFrom.b) / breakCount);
        colorArr.push(colorFrom);
        for (var i = 1; i < breakCount - 1; i++) {
            colorArr.push(new Color([colorFrom.r + (deltaR * i), colorFrom.g + (deltaG * i), colorFrom.b + (deltaB * i), opacity]));
        }
        colorArr.push(colorTo);
        return colorArr;
    }


    Util.getCrimeCoefficient = function (countCrime, regCode, parDate) {//коэффициента преступности
        var count_people = Const.Population[regCode]; //абсолютная численность населения в регионе
        if (!(count_people)) {
            count_people = 100000000;
        }
        var _year = parDate.getFullYear();
        var year_day = Math.ceil((new Date(_year + 1, 0, 1) - new Date(_year, 0, 1)) / (1000 * 60 * 60 * 24)); //число дней в году
        var count_day = Math.ceil((parDate - new Date(_year, 0, 1)) / (1000 * 60 * 60 * 24)); // число дней с начало года

        //коэффициента преступности = (число преступлений  * единица  расчета (на 10000 человек) * число дней в году)
        //                            / (численность населения * число дней с начало года)
        return Math.ceil((countCrime * 10000 * year_day) / (count_day * count_people));
    }

    Util.hasOwnFeatures = function (results) {//вернул ли ГИС сервер результат?
        return (results.hasOwnProperty("features") &&
            results.features.length > 0);

    }

    Util.addComboBoxOptions = function (combo, results) {//заполнение элемента select
        if (Util.hasOwnFeatures(results)) {
            for (var i = 0; i < results.features.length; i++) {
                var feature = results.features[i];
                combo.addOption({disabled: false, label: feature.attributes.NAME, selected: false, value: feature.attributes.ID});
            }
        } else {
            combo.addOption({disabled: false, label: "Значения не найдены", selected: false, value: -1});
        }
    }

    Util.resetComboBox = function (combo) {//очистить элемент select
        combo.removeOption(combo.getOptions());
        combo.addOption({disabled: false, label: "", selected: false, value: -1});
    }

    Util.toggleTileLayers = function (map, id, removeId, tileLayers) {//переключение между подложками
        if (map.getLayer(id) != undefined) {//Этот слой активен
            return;
        }
        map.removeLayer(tileLayers[removeId]);//удаляем старый базовый слой
        map.addLayer(tileLayers[id]); //добавляем новый базовый слой
        map.reorderLayer(tileLayers[id], 0); //поместить вниз
    }

    return Util;
});
